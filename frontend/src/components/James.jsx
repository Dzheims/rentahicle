import React, { useState, useEffect } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import rows from '@material-ui/core/TableRow';
import doAjax from './doAjax';
import TextField from '@material-ui/core/TextField';
import { Container, Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { useHistory } from 'react-router-dom'



function fetchCarSchedules(setSchedules, carId) {
    doAjax('GET', null, 'http://localhost:5000/get-car-schedules/' + carId, (schedule) => {
        setSchedules(schedule)
    })
}


const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },

}))(TableRow);


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignSelf: 'center-align'
    },
    cardGrid: {
      paddingTop: theme.spacing(8),
      paddingBottom: theme.spacing(8),
    }
}));


export default function James(props) {
    const classes = useStyles();
    const history = useHistory()
    const {car, userId} = props.history.location.state
    const carId = car.carId;
    console.log(props)

    const [values, setValues ] = useState({ schedules: [], car: null , date: {carId: carId, dateStart:"", dateEnd:""} })

    const handleChange = name => event => {
        setValues({...values, date: {...values.date, [name]: event.target.value}})
        console.log(values)
    }


    useEffect(() => {
        setValues({ ...values, car })

        fetchCarSchedules((schedules) => {
            setValues({ ...values, schedules })
        }, carId)

        console.log(values)

    }, [])


    console.log(values);

    return (
        <div>
            <Paper className={classes.root}>
                <Table className={classes.table} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell align="left">From</StyledTableCell>
                            <StyledTableCell align="left">To</StyledTableCell>
                            <StyledTableCell align="left">From</StyledTableCell>
                            <StyledTableCell align="left">To</StyledTableCell>
                            <StyledTableCell align="left">From</StyledTableCell>
                            <StyledTableCell align="left">To</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {values.schedules.map(date => (
                            <TableRow key={date}>
                                {console.log(date)}
                                <TableCell component="th" scope="row">
                                    {new Date(date.dateStart).toDateString()}
                                </TableCell>
                                <TableCell component="th" scope="row">
                                    {new Date(date.dateEnd).toDateString()}
                                </TableCell>
                            </TableRow>
                        ))}

                    </TableBody>
                </Table>

            </Paper>
            <div >
                <Grid container justify="space-around">
                    <form className={classes.container} noValidate>
                    <TextField
                        align="center"
                        id="date1"
                        label="Start: "
                        type="date"
                        defaultValue=''
                        value={values.dateStart}
                        className={classes.textField}
                        onChange =  {handleChange("dateStart")}
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                    </form>
                    <form className={classes.container} noValidate>
                    <TextField
                        id="date2"
                        label="End: "
                        type="date"
                        defaultValue=''
                        value={values.dateEnd}
                        className={classes.textField}
                        onChange =  {handleChange("dateEnd")}
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                    </form>
                </Grid>
            </div>
            <Container className={classes.cardGrid} maxWidth="xl" >
              <Grid container spacing={4} justify="center"> 
                <Grid item key={car} xs={100} sm={100} md={100}>
                    <Card className={classes.card}>
                    <CardMedia
                        className={classes.cardMedia}
                        image="https://source.unsplash.com/random"
                        title="Image title"
                    />
                    <CardContent className={classes.cardContent}>
                        <Typography gutterBottom variant="h5" component="h2">
                        {car.brand} {car.model}
                        </Typography>
                        <Typography>
                        Plate Number: {car.plateNo} 
                        <br/>
                        Daily Rate: {car.dailyRate}
                        
                        </Typography>
                    </CardContent>
                    {/* <CardActions>
                        <Button size="small" color="primary">
                        View
                        </Button>
                        <Button size="small" color="primary">
                        Schedule
                        </Button>
                    </CardActions> */}
                    </Card>
                </Grid>
            
              </Grid>
              <div className={classes.heroButtons}>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Button 
                    variant="contained" 
                    color="primary" 
                    size="large"
                    onClick= { ()=>{doAjax("POST", JSON.stringify({rental: {...values.date, userId}, }), "http://localhost:5000/rent", (success)=>{console.log(success)})}}
                  >
                    Reserve Date
                  </Button>
                </Grid>
              </Grid>
            </div>
            </Container>

        </div>
    );
}
