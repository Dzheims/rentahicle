import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import SaveIcon from '@material-ui/icons/Save';

const useStyles = makeStyles(theme => ({
    root: {

        padding: theme.spacing(3, 2),
    },
    paper: {
      height: 300,
      width: 600,
    },
    cardGrid: {
        paddingTop: theme.spacing(20),
        paddingBottom: theme.spacing(8),
      },


  }));


export default function SimpleContainer() {
    const [values, setValues] = React.useState({
        image: ''
    });

    const getBase64 = (file, cb) => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
    
    const onChangeHandler=event=>{
        const file = event.target.files[0]
        getBase64(file, result => {
            setValues({...values, image: result})
        })
    }

    const classes = useStyles();
    const description = 'description'
  return (
    <Container className={classes.cardGrid} maxWidth="sd">
    <Grid container className={classes.root} >
      <Grid item xs={12} >
        <Grid container justify="center">
            <img style={{height: 150, width: 150}} src={`${values.image}`} alt="avatar" />            
        </Grid>
        <Grid container justify="center">
              <input
                accept="image/*"
                className={classes.input}
                id="contained-button-file"
                type="file"
                onChange={onChangeHandler}                
            />
        </Grid>
        <Grid container justify="center">
            <TextField
            id="standard-uncontrolled"
            label="Description"
            className={classes.textField}
            margin="normal"
        />
        </Grid>
        <Grid container justify="center">
            <TextField
            label="Plate Number"
            className={classes.textField}
            margin="normal"
        />
        </Grid>
        <Grid container justify="center">
            <TextField
            label="Model"
            className={classes.textField}
            margin="normal"
        />
        </Grid>
        <Grid container justify="center">
            <TextField
            label="Brand"
            className={classes.textField}
            margin="normal"
        />
        </Grid>
        <Grid container justify="center" block>
        <Button 
            variant="contained"
            color="primary"
            size="large"
            className={classes.button}
            startIcon={<SaveIcon />}
            >
            Save
        </Button>
        </Grid>
      </Grid>
    </Grid>     
    </Container>
    
  );
}
