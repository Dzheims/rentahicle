import React, {Component, useState, useEffect} from 'react';
import Grid from '@material-ui/core/Grid';
import Calendar from 'react-calendar';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    icon: {
      marginTop: theme.spacing(2),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
      },
}));
export default function RentalForm(){
    const classes = useStyles();
    const [date, setDate] = useState({
        start: '',
        end: ''
      })
     
      useEffect(() => {
        console.log(date)
      }, [date])
      const startDate = new Date('10/19/2019')
      const endDate = new Date('10/25/2019')        
    const onChange = (name, dateInput) => setDate({...date, [name]: dateInput})
    return ( 
        <>
        <Grid className={classes.cardGrid}container justify="center">
            <h1>
            S<br/>
            T<br/>
            A<br/>
            R<br/>
            T<br/>
            </h1>
        <Calendar
          onChange={(dateInput) => onChange('start', dateInput)}
          value={date.start}
          tileDisabled={({activeStartDate, date, view }) => moment(date).isSameOrBefore(startDate) || moment(date).isSameOrAfter(endDate) ? false : true }
        />
        <Calendar
          onChange={(dateInput) => onChange('end', dateInput)}
          value={date.end}
          tileDisabled={({activeStartDate, date, view }) => moment(date).isSameOrBefore(startDate) || moment(date).isSameOrAfter(endDate) ? false : true }
        />
        <h1>
        
        <br/>
        E<br/>
        N<br/>
        D<br/>
        </h1>
      </Grid>
      <Grid container justify="center">
          <button>
            SCHEDULE
          </button>
      </Grid>
      </>
    )
}
