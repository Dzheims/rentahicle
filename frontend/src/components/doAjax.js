export default function doAjax(method, body, url, handler) {
    const xhr = new XMLHttpRequest()
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                handler(JSON.parse(xhr.responseText))
            }
        }
    }
    xhr.open(method, url, true)
    if (method === "GET") {

        xhr.send();
    } else if (method === "POST") {
        xhr.setRequestHeader('Content-Type', 'application/json')
        xhr.send(body)
    }
}