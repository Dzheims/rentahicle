import React from 'react';
// import logo from './logo.svg';
// import './App.css';
import {
  BrowserRouter, Switch, Route,
} from 'react-router-dom';
// import ReactDom from 'react-dom';

import SignIn from './components/User/SignIn';
import SignUp from './components/User/SignUp';
import Home2 from './components/Home2';
import Checkout from './components/bayad/Checkout';
import Review from './components/bayad/Review';
import RentalForm from './components/AdminPage/RentalForm';
import AddressForm from './components/bayad/AddressForm';
import Admin from './components/AdminPage/Admin';
import AddCar from './components/AdminPage/AddCar';
import JamesPage from './components/James';




function App() {
  return (
    <BrowserRouter>
      <div>
        <Switch>
          <Route path="/SignUp" component={SignUp} />
          <Route path="/SignIn" component={SignIn} />
          <Route path="/Home2" component={Home2} />
          <Route path="/AddressForm" component={AddressForm} />
          <Route path="/Checkout" component={Checkout} />
          <Route path="/Review" component={Review} />
          <Route path="/RentalForm" component={RentalForm} />
          <Route path="/Admin" component={Admin} />
          <Route path="/AddCar" component={AddCar} />
          <Route path="/JamesPage" component={JamesPage} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
