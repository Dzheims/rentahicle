const bcrypt = require('bcryptjs');

function hashPassword(plainText, doThisAfterHashing) {
    bcrypt.genSalt(10, (error, salt) => {
        bcrypt.hash(plainText, salt, doThisAfterHashing)
    })
}

module.exports = function signupUsers(app, db) {
    app
        .post('/signup', (request, response) => {
            const { firstName, lastName, age, contactNumber, address, email, password, isAdmin } = request.body;
            hashPassword(password, (hashError, hash) => {
                db.query('INSERT INTO users VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?)', [firstName, lastName, age, contactNumber, address, email, hash, isAdmin],

                    (dbError, result) => {
                        if (dbError) {
                            response.status(500);
                            response.json({ error: dbError.message })
                        } else {
                            response.json(result);
                        }
                    });
            })

        })
}