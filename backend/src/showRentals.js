module.exports = function showRentals(app, db) {
    app
        .get('/rentals', (request, response) => {
            db.query('SELECT * FROM rental',
                (dbError, result) => {
                    if (dbError) {
                        response.json({ error: dbError.message })

                    } else {
                        console.log(result)
                        response.json(result);
                    }
                });
        })
        .get('/show-rental-sched', (request, response) => {
            const { carId } = request.body;
            db.query('SELECT dateStart,dateEnd FROM rental INNER JOIN cars ON cars.carId = rental.carId where cars.carId = ?', [carId],
                (dbError, result) => {
                    if (dbError) {
                        response.json({ error: dbError.message })
                    } else {
                        response.json(result);
                    }
                });
        })
        .get('/get-rentals', (request, response) => {
            const { carId } = request.body;
            db.query('SELECT dateStart,dateEnd FROM rental INNER JOIN cars ON cars.carId = rental.carId where cars.carId = ?', [carId],
                (dbError, result) => {
                    if (dbError) {
                        response.json({ error: dbError.message })
                    } else {
                        response.json(result);
                    }
                });
        })
        .get('/checkDate', (request, response) => {
            const { carId, start, end } = request.body;
            db.query('SELECT dateStart,dateEnd FROM rental INNER JOIN cars ON cars.carId = rental.carId where cars.carId = ?', [carId],
                (dbError, result) => {
                    if (dbError) {
                        response.json({ error: dbError.message })
                    } else {
                        if (result.some(result => (start >= result.dateStart && start <= result.dateEnd))) {
                            if (result.some(result => (end >= result.dateStart && end <= result.dateEnd))) {
                                response.json({ conflict: true });
                                console.log("TRUEEE")
                            }
                            response.json({ conflict: true });
                        } else {
                            response.json({ conflict: false });
                        }

                        // console.log(result.some(result => result.dateStart >= start && result.dateEnd) );

                    }
                });
        })

}