module.exports = function reserveSchedule(app, db) {
    app
        .post('/rent', (request, response) => {
            console.log(request.body)
            const { carId, userId, dateStart, dateEnd } = request.body.rental;
            db.query('INSERT INTO rental VALUES (?,?,?,?)', [carId, userId, dateStart, dateEnd],
                (dbError, result) => {
                    if (dbError) {
                        response.json({ error: dbError.message })
                    } else {
                        response.json(result);
                    }
                });
        })
}