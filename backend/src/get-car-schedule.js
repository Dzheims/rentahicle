module.exports = function(app, db) {
    app
        .get('/get-car-schedules/:id', (request, response) => {
            db.query('SELECT dateStart,dateEnd FROM cars INNER JOIN rental ON rental.carId = cars.carId WHERE cars.carId = ?', [request.params.id],
                (dbError, result) => {
                    if (dbError) {
                        response.status(500);
                        response.json({ error: dbError.message })
                    } else {
                        response.json(result);
                    }
                });
        })
}