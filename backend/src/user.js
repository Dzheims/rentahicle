module.exports = function usersCRUD(app, db) {
    app
        .get('/user', (request, response) => {
            db.query('SELECT * FROM users', (dbError, result) => {
                if (dbError) {
                    response.status(500);
                    response.json({ error: dbError.message });
                } else {
                    response.json(result);
                }
            })
        })
        .get('/user/:id', (request, response) => {
            db.query('SELECT * FROM users WHERE id = ?', [request.params.id], (dbError, result) => {
                if (dbError) {
                    response.status(500);
                    response.json({ error: dbError.message });
                } else {
                    response.json(result);
                    response.json({ reason: 'Not found' })
                }
            });
        })

}