module.exports = function adminControl(app, db) {
    app
        .get('/admin/rent-schedules', (request, response) => {
            db.query('SELECT * FROM rent_schedules',
                (dbError, result) => {
                    if (dbError) {
                        response.json({ error: dbError.message })
                    } else {
                        response.json(result);
                    }
                });
        })

}