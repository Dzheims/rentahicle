module.exports = function addCar(app, db) {
    app
        .post('/cars', (request, response) => {
            const { plateNo, dailyRate, brand, model, seatCapacity, image } = request.body;
            db.query('INSERT INTO cars VALUES (NULL, ?,?,?,?,?,?)', [plateNo, dailyRate, brand, model, seatCapacity, image],
                (dbError, result) => {
                    if (dbError) {
                        response.status(500);
                        response.json({ error: dbError.message })
                    } else {
                        response.json(result);
                    }
                });
        })
}