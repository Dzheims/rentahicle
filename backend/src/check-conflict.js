const input = {
    dateStart: new Date('5/3/2019'),
    dateEnd: new Date('10/3/2019'),
}

const schedules = [{
    dateStart: new Date('11/3/2019'),
    dateEnd: new Date('15/3/2019')
}, ]

function checkConflict(input, schedules) {
    let isConflict = true
    if (schedules.some(result => (input.dateStart >= result.dateStart && input.dateStart <= result.dateEnd) || (input.dateEnd >= result.dateStart && input.dateEnd <= result.dateEnd))) {

        isConflict = false

    }
    return isConflict
}

console.log(checkConflict(input, schedules))