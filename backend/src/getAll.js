module.exports = function getAll(app, db) {
    app
        .get('/get-all', (request, response) => {
            db.query('SELECT * FROM cars INNER JOIN rental ON rental.carId = cars.carId',
                (carError, cars) => {
                    if (carError) {
                        response.status(500).send({ carError })
                    } else {
                        db.query('SELECT * FROM rental',
                            (rentalError, rentals) => {
                                if (rentalError) {
                                    response.status(500).send({ rentalError })
                                } else {
                                    response.json({ cars, rentals });
                                }
                            });

                    }
                });
        })
}