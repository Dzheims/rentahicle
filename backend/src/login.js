const bcrypt = require('bcryptjs');


module.exports = function login(app, db) {
    app
        .post('/auth', (request, response) => {
            db.query('SELECT * FROM users WHERE email = ? ', [request.body.email],
                (dbError, result) => {
                    if (dbError) {
                        console.log(dbError)
                    } else {
                        if (result.length === 0) {
                            response.json({ success: false })
                        } else {
                            bcrypt.compare(request.body.password, result[0].hashPassword, (compareError, correctPassword) => {
                                if (compareError) {
                                    response.json({ correctPassword: false })
                                } else {
                                    response.json({ correctPassword: true, user: result[0] })
                                }
                            })
                        }
                    }
                });
        })
}