module.exports = function getCars(app, db) {
    app
        .get('/get-cars', (request, response) => {
            db.query('SELECT * FROM cars',
                (dbError, result) => {
                    if (dbError) {
                        response.status(500);
                        response.json({ error: dbError.message })
                    } else {
                        response.json(result);
                    }
                });
        })
}