const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const mysql = require('mysql')

const addCar = require('./add-car')
const adminController = require('./admin-controller')
const getCars = require('./cars')
const login = require('./login')
const signUp = require('./signUp')
const rent = require('./reserve-schedule')


const uersCRUD = require('./user');
const showRentals = require('./showRentals')
const getAll = require('./getAll')
const getCarSchedule = require('./get-car-schedule')

const db = mysql.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '',
    database: 'rentahicle'
});

db.connect();

const app = express();
app
    .use(cors())
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())


rent(app, db);
signUp(app, db);
login(app, db);
adminController(app, db);
getCars(app, db)
addCar(app, db)

uersCRUD(app, db);
getAll(app, db);
getCarSchedule(app, db);
showRentals(app, db);


app.listen(5000, () => console.log('SIR MASKI TRES DLANG BI!'));
//5000 amo ni ang port