exports.up = function(knex) {
    return knex
        .schema
        .createTable('users', (table) => {
            table.increments('userId').primary()
            table.string('firstName', 255)
            table.string('lastName', 255)
            table.integer('age')
            table.string('contactNumber', 255)
            table.string('address', 255)
            table.string('email', 255)
            table.string('hashPassword', 1000)
            table.boolean('isAdmin')
        })
};

exports.down = function(knex) {
    return knex
        .schema
        .dropTable('users')

};