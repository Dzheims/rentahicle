exports.up = function(knex) {
    return knex
        .schema
        .createTable('rental', (table) => {
            table.integer('carId')
            table.integer('userId')
            table.date('dateStart')
            table.date('dateEnd')
        })
};

exports.down = function(knex) {
    return knex
        .schema
        .dropTable('rental')

};