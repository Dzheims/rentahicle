exports.up = function(knex) {
    return knex
        .schema
        .createTable('cars', (table) => {
            table.increments('carId').primary()
            table.string('plateNo').unique()
            table.integer('dailyRate')
            table.string('brand', 255)
            table.string('model', 255)
            table.integer('seatCapacity')
            table.string('image', 1000)
        })
};

exports.down = function(knex) {
    return knex
        .schema
        .dropTable('cars')

};